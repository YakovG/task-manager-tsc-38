package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

import java.sql.Connection;

public final class UserRepositoryTest implements ServiceLocator {

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestUserTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        userRepository.clear();
        connection.commit();
        connection.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userRepository.removeOneById(userById.getId()));
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveByLogin() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getLogin());
        Assert.assertEquals("test",userById.getLogin());
        Assert.assertNotNull(userRepository.removeUserByLogin("test"));
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveByEmail() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getEmail());
        Assert.assertEquals("test@test",userById.getEmail());
        Assert.assertNotNull(userRepository.removeUserByEmail("test@test"));
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        final User user1 = new User();
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user1.getId());
        user1.setLogin("user");
        Assert.assertNotNull(user1.getLogin());
        Assert.assertEquals("user", user1.getLogin());
        user1.setPasswordHash("user");
        Assert.assertNotNull(user1.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user1));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.clear();
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        final User user1 = new User();
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user1.getId());
        user1.setLogin("user");
        Assert.assertNotNull(user1.getLogin());
        Assert.assertEquals("user", user1.getLogin());
        user1.setPasswordHash("user");
        Assert.assertNotNull(user1.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user1));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals(2,userRepository.size());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFind() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        Assert.assertNotNull(userRepository.add(user));
        connection.commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertNotNull(userRepository.findUserByLogin("test"));
        Assert.assertEquals("test", userRepository.findUserByLogin("test").getLogin());
        Assert.assertNotNull(userRepository.findUserByEmail("test@test"));
        Assert.assertEquals("test@test", userRepository.findUserByEmail("test@test").getEmail());
        connection.close();
    }
}
