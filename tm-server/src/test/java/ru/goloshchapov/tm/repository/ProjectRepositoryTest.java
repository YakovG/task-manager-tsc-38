package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

import java.sql.Connection;
import java.util.Date;

public final class ProjectRepositoryTest implements ServiceLocator{

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestProjectTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        projectRepository.clear();
        connection.commit();
        connection.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        Assert.assertNotNull(projectRepository.add(project));
        connection.commit();
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        final Project projectById = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        Assert.assertNotNull(projectRepository.add(project));
        connection.commit();
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        final Project projectById = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        Assert.assertNotNull(projectRepository.removeOneById(projectById.getId()));
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        Assert.assertNotNull(projectRepository.add(project));
        connection.commit();
        Assert.assertNotNull(projectRepository.add(project1));
        connection.commit();
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.clear();
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertNotNull(projectRepository.add(project1));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(2,projectRepository.size());
        connection.close();
    }
}
