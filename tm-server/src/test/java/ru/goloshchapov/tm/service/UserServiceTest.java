package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.UserRepository;

import java.sql.Connection;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public final class UserServiceTest implements ServiceLocator{

    private Session session;

    private ServiceLocator serviceLocator;

    private User user;

    private ConnectionService connectionService;

    private UserService userService;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }
    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    public @NotNull IConnectionService getConnectionService() {
        return connectionService;
    }

    @Before
    public void before() {
        serviceLocator = this;
        session = new Session();
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        user = new User();
        user.setId("uuu");
        user.setLogin("user");
        final String password = "user";
        user.setPasswordHash(salt(propertyService, password));
        user.setEmail("user@user");
        userService = new UserService(serviceLocator);
        userService.add(user);
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        final UserRepository userRepository = new UserRepository(connection);
        userRepository.clear();
        connection.commit();
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSetPassword() {
        final UserService userService = new UserService(serviceLocator);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertNotNull(userService.findOneById("uuu").getPasswordHash());
        @Nullable final String passwordOld = salt(propertyService, "user");
        Assert.assertNotNull(passwordOld);
        @Nullable User user1 = userService.findOneById("uuu");
        Assert.assertNotNull(user1);
        Assert.assertEquals(passwordOld,user1.getPasswordHash());
        Assert.assertNotNull(userService.setPassword("uuu", "password"));
        Assert.assertNotNull(userService.findOneById("uuu").getPasswordHash());
        @Nullable final String passwordNew = salt(propertyService, "password");
        Assert.assertNotNull(passwordNew);
        Assert.assertEquals(passwordNew,userService.findOneById("uuu").getPasswordHash());
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testUpdateUser() {
        final UserService userService = new UserService(serviceLocator);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertNull(userService.findOneById("uuu").getFirstname());
        Assert.assertNull(userService.findOneById("uuu").getLastname());
        Assert.assertNull(userService.findOneById("uuu").getMiddlename());
        Assert.assertNotNull(
                userService.updateUser("uuu","firstname", "lastname", "middlename" )
        );
        Assert.assertNotNull(userService.findOneById("uuu").getFirstname());
        Assert.assertNotNull(userService.findOneById("uuu").getLastname());
        Assert.assertNotNull(userService.findOneById("uuu").getMiddlename());
        Assert.assertEquals("firstname",userService.findOneById("uuu").getFirstname());
        Assert.assertEquals("lastname",userService.findOneById("uuu").getLastname());
        Assert.assertEquals("middlename",userService.findOneById("uuu").getMiddlename());
    }

    @Test
    @Category(DataCategory.class)
    public void testLockUnlockUserByLogin() {
        final UserService userService = new UserService(serviceLocator);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertEquals("user", userService.findOneById("uuu").getLogin());
        Assert.assertNotNull(userService.lockUserByLogin("user"));
        Assert.assertNotNull(userService.findUserByLogin("user"));
        Assert.assertEquals(userService.findOneById("uuu").getId(), userService.findUserByLogin("user").getId());
        Assert.assertTrue(userService.findUserByLogin("user").isLocked());
        Assert.assertNotNull(userService.unlockUserByLogin("user"));
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertEquals("user", userService.findOneById("uuu").getLogin());
        Assert.assertNotNull(userService.findUserByLogin("user"));
        Assert.assertEquals(userService.findOneById("uuu").getId(), userService.findUserByLogin("user").getId());
        Assert.assertFalse(userService.findUserByLogin("user").isLocked());
    }

    @Test
    @Category(DataCategory.class)
    public void testLockUnlockUserByEmail() {
        final UserService userService = new UserService(serviceLocator);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertEquals("user@user", userService.findOneById("uuu").getEmail());
        Assert.assertNotNull(userService.lockUserByEmail("user@user"));
        Assert.assertNotNull(userService.findUserByEmail("user@user"));
        Assert.assertEquals(userService.findOneById("uuu").getId(), userService.findUserByEmail("user@user").getId());
        Assert.assertTrue(userService.findUserByEmail("user@user").isLocked());
        Assert.assertNotNull(userService.unlockUserByEmail("user@user"));
        Assert.assertNotNull(userService.findOneById("uuu"));
        Assert.assertEquals("user@user", userService.findOneById("uuu").getEmail());
        Assert.assertNotNull(userService.findUserByEmail("user@user"));
        Assert.assertEquals(userService.findOneById("uuu").getId(), userService.findUserByEmail("user@user").getId());
        Assert.assertFalse(userService.findUserByEmail("user@user").isLocked());
    }
}
