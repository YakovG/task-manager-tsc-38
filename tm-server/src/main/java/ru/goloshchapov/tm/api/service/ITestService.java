package ru.goloshchapov.tm.api.service;

import java.sql.Connection;

public interface ITestService {

    Connection getConnection();

    void initTestUserTable();

    void initTestSessionTable();

    void initTestProjectTable();

    void initTestTaskTable();

    void dropDatabase();
}
