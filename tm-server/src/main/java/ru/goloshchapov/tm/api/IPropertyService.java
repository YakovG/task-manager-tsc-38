package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull String getApplicationVersion();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @NotNull String getSessionSalt();

    @NotNull Integer getSessionCycle();

    @NotNull String getJdbcUsername();

    @NotNull String getJdbcPassword();

    @NotNull String getJdbcUrl();

    String getJdbcTestUsername();

    String getJdbcTestPassword();

    String getJdbcTestUrl();
}
