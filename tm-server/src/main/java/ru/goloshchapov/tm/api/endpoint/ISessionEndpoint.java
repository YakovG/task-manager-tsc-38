package ru.goloshchapov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Result;
import ru.goloshchapov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {
    @WebMethod
    @Nullable Session openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    @NotNull Result closeSession(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    @NotNull Result closeSessionByLogin(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );
}
