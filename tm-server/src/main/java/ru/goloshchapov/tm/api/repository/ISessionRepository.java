package ru.goloshchapov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull Session merge (@NotNull Session session);

    boolean contains(String sessionId);

    void exclude(@NotNull Session session);

    @Nullable Session findByUserId (String userId);

    List<Session> findAllSession();
}
