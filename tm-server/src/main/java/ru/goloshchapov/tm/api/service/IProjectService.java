package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.repository.ProjectRepository;

import java.sql.Connection;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull ProjectRepository getRepository(@NotNull Connection connection);

    @NotNull Project add(String userId, String name, String description);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project project);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String projectId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String projectName);
}
