package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;
import java.util.List;

public interface IProjectEndpoint {
    @WebMethod
    @SneakyThrows
    @Nullable List<Project> findProjectAll(
            @WebParam(name = "session") Session session);

    @WebMethod
    @SneakyThrows
    void addProjectAll(
            @WebParam(name = "session") Session session,
            @WebParam(name = "collection") Collection<Project> collection
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project addProject(
            @WebParam(name = "session") Session session,
            @WebParam(name = "project") Project model
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Project> findAllProjectByUserId(
            @WebParam(name = "session") Session session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project findProjectByNameAndUserId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name);

    @WebMethod
    @SneakyThrows
    int sizeProject(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    void removeProject(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") Project model
    );

    @WebMethod
    @SneakyThrows
    void clearProject(
            @WebParam(name = "session") Session session
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project removeProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project removeProjectByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project removeProjectByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project startProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project startProjectByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project startProjectByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project finishProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String modelId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project finishProjectByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "index") Integer index
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project finishProjectByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "name") String name
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Project> sortedProjectBy(
            @WebParam(name = "session") Session session,
            @WebParam(name = "sortCheck") String sortCheck
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project updateProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String modelId,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project updateProjectByIndex(@WebParam(name = "session") Session session,
                                 @WebParam(name = "index") Integer index,
                                 @WebParam(name = "name") String name,
                                 @WebParam(name = "description") String description
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project changeProjectStatusById(@WebParam(name = "session") Session session,
                                    @WebParam(name = "projectId") String id,
                                    @WebParam(name = "statusChange") String statusChange
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project changeProjectStatusByName(@WebParam(name = "session") Session session,
                                      @WebParam(name = "name") String name,
                                      @WebParam(name = "statusChange") String statusChange
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Project changeProjectStatusByIndex(@WebParam(name = "session") Session session,
                                       @WebParam(name = "index") int index,
                                       @WebParam(name = "statusChange") String statusChange
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project addProjectByName(@WebParam(name = "session") Session session,
                                      @WebParam(name = "name") String name,
                                      @WebParam(name = "description") String description
    );

    @WebMethod
    @SneakyThrows
    void checkProjectAccess(
            @WebParam(name = "session") Session session
    );
}
