package ru.goloshchapov.tm.constant;

public interface TableConst {

    String TABLE_PREFIX = "tm_";

    String SESSION_TABLE = TABLE_PREFIX + "session";

    String USER_TABLE = TABLE_PREFIX + "user";

    String PROJECT_TABLE = TABLE_PREFIX + "project";

    String TASK_TABLE = TABLE_PREFIX + "task";

}
