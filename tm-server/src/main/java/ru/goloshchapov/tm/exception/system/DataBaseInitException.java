package ru.goloshchapov.tm.exception.system;

import ru.goloshchapov.tm.exception.AbstractException;

public class DataBaseInitException extends AbstractException {

    public DataBaseInitException() {
        super("Error! Database is unavailable...");
    }
}
