package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.IProjectService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.repository.ProjectRepository;

import java.sql.Connection;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NotNull private final ServiceLocator serviceLocator;

    @NotNull
    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull public final ProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project result = projectRepository.add(userId, project);
            connection.commit();
            return result;
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public Project update(@Nullable Project project) {
        if (project == null) return null;
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getRepository(connection);
            @NotNull final Project result = projectRepository.update(project);
            connection.commit();
            return result;
        } catch (final Exception e) {
            connection.rollback();
            throw  e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentById(@NotNull String userId, @NotNull String projectId) {
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getRepository(connection);
            return projectRepository.isAbsentById(userId, projectId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentByName(@NotNull String userId, @NotNull String projectName) {
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getRepository(connection);
            return projectRepository.isAbsentByName(userId, projectName);
        } finally {
            connection.close();
        }
    }
}

