package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final ServiceLocator serviceLocator;

    public AbstractService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract IRepository<E> getRepository(@NotNull final Connection connection);

    public Timestamp prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        @NotNull final Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;
    }

    public java.util.Date prepare(@Nullable final Timestamp timestamp) {
        if (timestamp == null) return null;
        @NotNull final java.util.Date date = new Date(timestamp.getTime());
        return date;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.addAll(collection);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            final E result = repository.add(entity);
            connection.commit();
            return result;
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            @Nullable List<E> entities = repository.findAll();
            if (entities == null) throw new ElementsNotFoundException();
            return entities;
        } catch (final Exception e) {
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findOneById(id);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@Nullable final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentById(@Nullable final String id) {
        if (isEmpty(id)) return true;
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.isAbsentById(id);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentByIndex(@Nullable final Integer index) {
        if (!checkIndex(index, size())) return true;
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.isAbsentByIndex(index);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@Nullable final Integer index) {
        if (!checkIndex(index, size())) throw new IndexIncorrectException();
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.getIdByIndex(index);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int size() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.size();
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            final E entity = repository.removeOneById(id);
            connection.commit();
            return entity;
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByIndex(@Nullable final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            final E entity = repository.removeOneByIndex(index);
            connection.commit();
            return entity;
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
