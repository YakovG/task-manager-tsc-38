package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ITaskService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.repository.TaskRepository;


import java.sql.Connection;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull private final ServiceLocator serviceLocator;

    @NotNull
    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull public final TaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final TaskRepository taskRepository = getRepository(connection);
            @Nullable final Task result = taskRepository.add(userId, task);
            connection.commit();
            return result;
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task update(@Nullable Task task) {
        if (task == null) return null;
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final TaskRepository taskRepository = getRepository(connection);
            @NotNull final Task result = taskRepository.update(task);
            connection.commit();
            return result;
        } catch (final Exception e) {
            connection.rollback();
            throw  e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentById(@NotNull String userId, @NotNull String taskId) {
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final TaskRepository taskRepository = getRepository(connection);
            return taskRepository.isAbsentById(userId, taskId);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isAbsentByName(@NotNull String userId, @NotNull String taskName) {
        @NotNull final Connection connection = serviceLocator.getConnectionService().getConnection();
        try {
            @NotNull final TaskRepository taskRepository = getRepository(connection);
            return taskRepository.isAbsentByName(userId, taskName);
        } finally {
            connection.close();
        }
    }
}
