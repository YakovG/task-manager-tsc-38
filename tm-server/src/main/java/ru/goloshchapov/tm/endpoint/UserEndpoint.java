package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements ru.goloshchapov.tm.api.endpoint.IUserEndpoint {

    private final ServiceLocator serviceLocator;

    public UserEndpoint(ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User setUserPassword(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User updateUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, middleName);
    }


    @Override
    @WebMethod
    @SneakyThrows
    public User getUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findOneById(session.getUserId());
    }
}
