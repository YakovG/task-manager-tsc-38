package ru.goloshchapov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.other.ISaltSetting;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final ISaltSetting setting,
            @Nullable final String value
    ) {
        if (setting == null) return null;
        @Nullable String secret = setting.getPasswordSecret();
        @Nullable Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    static String salt(@Nullable final String secret,
                       @Nullable final Integer iteration,
                       @Nullable final String value) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i=0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
