package ru.goloshchapov.tm.comparator;

import ru.goloshchapov.tm.entity.IHaveDateStart;

import java.util.Comparator;

public final class ComparatorByDateStart implements Comparator<IHaveDateStart> {

    private static final ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    private ComparatorByDateStart() {
    }

    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHaveDateStart o1, final IHaveDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
