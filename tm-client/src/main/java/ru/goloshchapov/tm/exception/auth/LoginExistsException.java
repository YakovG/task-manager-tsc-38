package ru.goloshchapov.tm.exception.auth;

import ru.goloshchapov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() { super("Error! Login already exists..."); }

}
